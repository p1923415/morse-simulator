# morse-simulator

# Projet d'Orientation en Master : Human-Robot Interaction

## Project description
Implementation of different types of HRIs using Morse Simulator.

## Prerequisites
Install Morse Simulator here :
https://www.openrobots.org/morse/doc/stable/user/installation.html

## How To Use
Visit different branches (nils / paul) to test the different situations.
