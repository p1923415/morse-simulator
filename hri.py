from morse.builder import *

# Add a robot with a position sensor and a motion controller
human = Human()
r2d2 = ATRV()

keyboard = Keyboard()
human.append(keyboard)
#F5 to change if you want to control the Human or the Camera.
# Manipulation Mode, press X

#### POSE ####
robotPose = Pose()
robotPose.add_interface('ros')
r2d2.append(robotPose)

humanPose = Pose()
humanPose.add_interface('ros')
r2d2.append(humanPose)

#camera for human control
depthcamera = DepthCamera()
human.append(depthcamera)

# define one or several communication interface
depthcamera.add_interface('ros')

### MOTION ####
robotMotion = Waypoint()
robotMotion.add_interface('ros')
r2d2.append(robotMotion)

humanMotion = Waypoint()
humanMotion.add_interface('ros')
r2d2.append(humanMotion)

# Environment
env = Environment('land-1/trees')